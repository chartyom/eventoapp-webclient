import React from 'react';
import { Router, Route, Link, browserHistory } from 'react-router';

import DefaultTheme from '../layouts/themes/DefaultTheme';

import MainDefaultArticle from '../layouts/articles/main/MainDefaultArticle';
import AboutDefaultArticle from '../layouts/articles/about/AboutDefaultArticle';
import ErrorDefaultArticle from '../layouts/articles/error/ErrorDefaultArticle';

import ExampleBasicForm from '../layouts/forms/ExampleBasicForm';

const routes = (
    <Router history={browserHistory}>
        <Route component={DefaultTheme}>
            <Route path="/" component={MainDefaultArticle} />
            <Route path="/feedback" component={ExampleBasicForm} />
            <Route path="/about" component={AboutDefaultArticle} />
            <Route path="*" component={ErrorDefaultArticle} />
        </Route>
    </Router>
);

export default routes;