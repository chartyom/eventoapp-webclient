import React from 'react';
import { Link } from 'react-router';

export default React.createClass({
    render: function () {
        return (
            <div className={'container error-page error-page--' + (this.props.theme || 'default') }>

                    <div className="error-page__elem col-xs-12">
                        <h1>Страница не найдена</h1>
                        <h2>Возможно эта страница была удалена или никогда не создавалась</h2>
                        <p><Link to="/" className="btn btn-default">Вернуться на главную</Link></p>
                    </div>

            </div>
        );
    }
});