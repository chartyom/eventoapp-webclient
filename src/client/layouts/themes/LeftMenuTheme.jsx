import React from 'react';
import NavbarDefaultHeader from '../headers/NavbarDefaultHeader';

export default React.createClass({
  render: function () {
    return (
      <div className="app__main app__main--left-menu">
        <header>
          <NavbarDefaultHeader />
        </header>
        <article>
          <div className="app__main__left-content">
            <ul className="list-group">
              <li>
                <a href="#" className="list-group-item active">
                  Cras justo odio
                </a>
              </li>
              <a href="#" className="list-group-item">Dapibus ac facilisis in</a>
              <a href="#" className="list-group-item">Morbi leo risus</a>
              <a href="#" className="list-group-item">Porta ac consectetur ac</a>
              <a href="#" className="list-group-item">Vestibulum at eros</a>
            </ul>
          </div>
          <div className="app__main__right-content">
            {this.props.children}
          </div>
        </article>
      </div>
    );
  }
});