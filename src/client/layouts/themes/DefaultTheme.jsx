import React from 'react';
import NavbarDefaultHeader from '../headers/NavbarDefaultHeader';
import MainDefaultFooter from '../footers/MainDefaultFooter';

export default React.createClass({ 
  render: function () {
    return (
      <div className="app__main app__main--default">
        <header>
          <NavbarDefaultHeader />
        </header>
        <article>
          <div className="app__main__content">
            {this.props.children}
          </div>
        </article>
        <footer>
          <MainDefaultFooter />
        </footer>
      </div>
    );
  }
});