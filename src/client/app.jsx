import 'bootstrap';
//import 'swiper/dist/js/swiper.min.js';
import ReactDOM from 'react-dom';
import routes from './routes';

ReactDOM.render(routes, document.getElementById('app'));