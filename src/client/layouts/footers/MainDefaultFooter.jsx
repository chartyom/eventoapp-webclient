import React from 'react';

export default React.createClass({
  render: function () {
    var mdate = new Date(); 
    let year = mdate.getFullYear();
    return (
      <div className="main-footer main-footer--default">
        <div className="container">
          <div className="row">
            <div className="col-md-6">
              <div className="main-footer__design">
                WebClient by Chernyaev Artyom
              </div>
            </div>
            <div className="col-md-6">
              <div className="main-footer__copyright">
                Copyright © {year}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
});