# MyEvento WebClient
## Start 

``
  npm install 
``

``
  npm install -g
``

## Start production

for linux:

``
  sudo npm run build-linux-prod
``

``
  sudo npm start
``
 OR 
``
  sudo run start-linux-cluster-prod
``

for windows:

``
  npm run build-windows-prod
``

``
  npm run start-windows-prod
``
 OR 
``
  sudo run start-windows-cluster-prod
``

## Start development

the first console:

``
  npm run babel-dev
``

second console:

``
  npm run build-css-dev
``

third console:

``
  npm run build-js
``

fourth console:

``
  npm run start-server-dev
``

## Stop server

``
  npm stop
``

## More information

Для запуска процессов:

PM2 - https://www.npmjs.com/package/pm2