var bodyParser = require('body-parser'),
    React = require('react'),
    ReactDOM = React.DOM;

//var ReactApp = React.createFactory(AppClient);
import RoutesGenerator from '../utils/RoutesGenerator';

module.exports = function (app) {

    // create text/html parser 
    var textParser = bodyParser.text();
    app.get('/', function (req, res) {
        RoutesGenerator(req, res, {
            description: "Приложение для моментального оповещения о предстоящих событиях"
        });
    });
    app.get('/about', function (req, res) {
        RoutesGenerator(req, res, {
            title: "О проекте",
            description: "О проекте Evento"
        });
    });
    app.get('/feedback', function (req, res) {
        RoutesGenerator(req, res, {
            title: "Обратная связь",
            description: "Свяжитесь с нами"
        });
    });
    app.get('*', function (req, res) {
        RoutesGenerator(req, res, {
            title: "Страница не найдена",
            description: "Страница не найдена"
        });
    });
    return app;
};

