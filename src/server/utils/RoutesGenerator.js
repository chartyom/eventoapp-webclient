var React = require('react'),
    ReactDOM = React.DOM;
import { renderToString } from 'react-dom/server';
import routes from '../../client/routes';
import { match, RouterContext } from 'react-router';
import config from '../config';

export default (req, res, options) => {
    if (!options) options = {};
    options.title = options.title
        ? config.get('service:name') + ' ' + config.get('service:separator') + " " + options.title
        : config.get('service:name');
    options.description = (options.description || config.get('service:name'));
    // Note that req.url here should be the full URL path from
    // the original request, including the query string.
    match({ routes, location: req.url }, (error, redirectLocation, renderProps) => {
        if (error) {
            console.error('ERROR 500: ' + error.message);
            res.status(500).send(error.message);
        } else if (redirectLocation) {
            res.redirect(302, redirectLocation.pathname + redirectLocation.search);
        } else if (renderProps) {
            // You can also check renderProps.components or renderProps.routes for
            // your "not found" component or route respectively, and send a 404 as
            // below, if you're using a catch-all route.
            //res.status(200).send();
            res.render('main', {
                webAppVersion: config.get('versions:webApp'),
                title: options.title,
                description: options.description,
                content: renderToString(<RouterContext {...renderProps} />)
            });
        } else {
            // В случае возникновения ошибки требуется уведомить администратора
            // Необходимо: выдать JSON ошибку, либо редирект на главную
            console.error('ERROR 404: ' + req.url);
            res.status(404).send('Not found');
        }
    });
};
// A utility function to safely escape JSON for embedding in a <script> tag
function safeStringify(obj) {
    return JSON.stringify(obj).replace(/<\/script/g, '<\\/script').replace(/<!--/g, '<\\!--');
}