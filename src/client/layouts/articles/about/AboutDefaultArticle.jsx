import React from 'react';

export default React.createClass({
  render: function () {
    return (
      <div className="about-article about-article--default">
        <div className="container clearfix">
            <div className="col-xs-12">
              <h1>О проекте</h1>
              <p>Приложение <b>Evento</b> было создано с целью размещения событий организаторами и упрощения отслеживания событий обычными пользователями.</p>
              
            </div>
            <div className="col-xs-12">
              <h3 className="about-article__main-title"><b>Evento</b> имеет следующие возможности:</h3>
              <div className="col-xs-12 col-md-6">
                <div className="about-article__title">
                  Первая возможность
                </div>
                <div className="about-article__text">
                  Описание первой возможности
                </div>
              </div>
              <div className="col-xs-12 col-md-6">
                <div className="about-article__picture about-article__picture--right">
                  <img src="/img/opportunitys.png" alt="Первая возможность" />
                </div>
              </div>
              
              <div className="col-xs-12 col-md-6">
                <div className="about-article__picture about-article__picture--left">
                  <img src="/img/opportunitys.png" alt="Вторая возможность" />
                </div>
              </div>
              <div className="col-xs-12 col-md-6">
                <div className="about-article__title">
                  Вторая возможность
                </div>
                <div className="about-article__text">
                  Описание второй возможности
                </div>
              </div>
            </div>
            
            {this.props.children}
        </div>
      </div>
    );
  }
});
