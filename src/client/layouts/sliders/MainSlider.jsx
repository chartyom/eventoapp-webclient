import React from 'react';
/*
Site help: http://idangero.us/swiper/get-started
*/
export default React.createClass({
    componentDidMount() {
        this.drawSlider();
    },

    componentDidUpdate() {
        this.drawSlider();
    },

    drawSlider() {
        var swiper = new Swiper('.main-slider', {
            pagination: '.main-slider-pagination',
            nextButton: '.main-slider-button-next',
            prevButton: '.main-slider-button-prev',
            paginationClickable: true,
            centeredSlides: true,
            //autoplay: 25000,
            autoplayDisableOnInteraction: false,
            parallax: true,
            speed: 600
        });
    },

    render: function () {

        return (
            <div className="main-slider swiper-container">
                <div className="swiper-wrapper">
                    <div className="main-slider__item swiper-slide">
                        <div className="main-slider__item__container main-slider__item__slide1  clearfix">
                            <div className="main-slider__item__title" data-swiper-parallax="-100">Организовать встречу стало просто</div>
                            <div className="main-slider__item__text" data-swiper-parallax="-200">
                                <p>Приложение <b>EventoApp</b> поможет вам быстро организовать встречу или мероприятие, а так же держать всех участников в курсе последних событий с помощью моментальных уведомлений</p>
                            </div>
                        </div>
                    </div>
                    <div className="main-slider__item swiper-slide">
                        <div className="main-slider__item__container">
                            <div className="main-slider__item__title" data-swiper-parallax="-100">Slide 1</div>
                            <div className="main-slider__item__subtitle" data-swiper-parallax="-200">Subtitle</div>
                            <div className="main-slider__item__text" data-swiper-parallax="-300">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam dictum mattis velit, sit amet faucibus felis iaculis nec. Nulla laoreet justo vitae porttitor porttitor. Suspendisse in sem justo. Integer laoreet magna nec elit suscipit, ac laoreet nibh euismod. Aliquam hendrerit lorem at elit facilisis rutrum. Ut at ullamcorper velit. Nulla ligula nisi, imperdiet ut lacinia nec, tincidunt ut libero. Aenean feugiat non eros quis feugiat.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="main-slider-pagination swiper-pagination"></div>
                <div className="main-slider-button-next swiper-button-next">
                    <span className="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                </div>
                <div className="main-slider-button-prev swiper-button-prev">
                    <span className="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                </div>
            </div>
        );
    }
});