const nconf = require('nconf');
const path = require('path');

nconf.argv()
    .env(['process.env'])
    .file({ file: path.join(process.cwd(), 'config/settings.json') });

export default nconf;