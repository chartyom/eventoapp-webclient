// Server Loader
var express = require('express'),
    server = require('./server'),
    routes = require('./routes'),
    hbs        = require('hbs'),
    path = require('path'),
    app;

import config from './config';
import ClientInfo from './utils/ClientInfo';

app = express();

// Initialise
module.exports = {
    start: function () { 
        //Secure
        app.disable('x-powered-by');
        //Add view engine
        app.set('view engine', 'hbs');
        app.set('views', path.join(process.cwd(), 'views'));
        //Handlebars layouts
        //hbs.registerPartials('../../views');
        //Public files
        app.use(express.static(config.get("paths:content")));
        //Client information (ip,...)
        app.use(ClientInfo.initialize());
        // Routing
        routes(app);
        // Start Server
        (new server(app)).start();
    }
};
