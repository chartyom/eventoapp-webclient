import React from 'react';

/*
  import MainSlider from '../../sliders/MainSlider';
  ...
  <div className="main-article__slider-container">
    <MainSlider />
  </div>
*/
export default React.createClass({
  render: function () {
    return (
      <div className="main-article main-article--default">
        <div className="main-article__container clearfix">
            <div className="col-xs-12 col-md-6">
              <div className="main-article__container__description">
                <div className="main-article__container__description__title">Следить за событиями стало просто</div>
                <div className="main-article__container__description__text">
                  <p>Приложение <b>Evento</b> позволяет следить за событиями ваших друзей, акциями на товары в магазинах, подписываться на обновления, оценивать, а так же учавствовать в событиях.</p>
                  <p>С приложением <b>Evento</b> Вы всегда будете в курсе событий.</p>
                </div>
                <div className="main-article__container__description__download">
                  <div className="row">
                    <div className="col-md-12 main-article__container__description__download__title">
                      Скачайте приложение.
                    </div>
                    <div className="col-xs-6 main-article__container__description__download__left">
                      <a href="/" target="_blank">
                        <img src="/img/appstore.png" alt="Download on the App Store" />
                      </a>
                    </div>
                    <div className="col-xs-6 main-article__container__description__download__right">
                      <a href="/" target="_blank">
                        <img src="/img/googleplay.png" alt="Get it on Google play" />
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-xs-12 col-md-6">
              <div className="main-article__container__slide">
                <img src="/img/slide1.png" alt="slide1" />
              </div>
            </div>
            {this.props.children}
        </div>
      </div>
    );
  }
});
